import './plans.view.html'
import './formPlan.view.html'

class PlansController {
  constructor (PlanService, $stateParams, $state, $rootScope) {
    this._PlanService = PlanService
    this._$stateParams = $stateParams
    this._$state = $state
    this._$rootScope = $rootScope
    this.sku = $stateParams.platform
    this.user = {}
    if (this.sku)
      this.getPlans(this.sku)
  }

  getPlans (sku) {
    this._PlanService.getPlans(sku)
      .then(response => {
        this.plans = response.data.planos
      })
  }

  goSubmit (plan) {
    this.plan = plan
    this._$rootScope.planSelected = plan
    this._$state.go('plansFinish')
  }
 
  submitPlan (user) {
      console.log(this.user)
      console.log(this._$rootScope.planSelected)
  }

}

PlansController.$inject = ['PlanService', '$stateParams', '$state', '$rootScope']

export { PlansController }
