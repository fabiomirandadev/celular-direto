import './home.view.html'

class HomeController {
  constructor (PlanService) {
    this._PlanService = PlanService
    this.getPlatforms()
  }
  
  getPlatforms () {
    this._PlanService.getPlatforms()
      .then(response => {
        this.platforms = response.data.plataformas
      })
  }

}

HomeController.$inject = ['PlanService']

export { HomeController }
