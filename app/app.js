import './styles/main.styl'

import { Router } from './router.js'
import { PlanService } from './services/plan.service.js'
import { HomeController } from './components/home/home.controller.js'
import { PlansController } from './components/plans/plans.controller.js'

let app = angular.module('App', ['ui.router'])

Router.configure(app)

app
  .service('PlanService', PlanService)
  .controller('HomeController', HomeController)
  .controller('PlansController', PlansController)
