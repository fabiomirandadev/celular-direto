class PlanService {
  constructor ($http) {
    this._$http = $http
  }

  getPlatforms () {
    return this._$http.get('http://private-59658d-celulardireto2017.apiary-mock.com/plataformas')
  }

  getPlans (sku) {
    return this._$http.get(`http://private-59658d-celulardireto2017.apiary-mock.com/planos/${sku}`)
  }
}

PlanService.$inject = ['$http']
export { PlanService }
