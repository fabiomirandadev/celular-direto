class Router {
  static configure (app) {
    app.config(($stateProvider, $urlRouterProvider) => {
      $urlRouterProvider.otherwise('/')
      $stateProvider
        .state('main', {
          url: '',
          templateUrl: 'components/home/home.view.html',
          controller: 'HomeController as ctrl'
        })
        .state('plans', {
          url: '/planos/:platform',
          templateUrl: 'components/plans/plans.view.html',
          controller: 'PlansController as ctrl',
          params: {
            platform: null
          }
        })
        .state('plansFinish', {
          url: '/planos/submit',
          templateUrl: 'components/plans/formPlan.view.html',
          controller: 'PlansController as ctrl'
        })
    })
  }
}
export { Router }
